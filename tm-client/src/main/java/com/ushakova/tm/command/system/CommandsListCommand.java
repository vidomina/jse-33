package com.ushakova.tm.command.system;

import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Collection;

public class CommandsListCommand extends AbstractCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show commands.";
    }

    @Override
    public void execute() {
        @NotNull final Collection<AbstractCommand> commands = endpointLocator.getCommandService().getCommands();
        System.out.println("All commands:");
        for (@NotNull final AbstractCommand command : commands) {
            @NotNull final String name = command.name();
            if (name == null) continue;
            System.out.println(name);
        }
    }

    @Override
    @NotNull
    public String name() {
        return "commands";
    }

}