package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectUpdateProjectByIndexCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Update project by index.";
    }

    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("***Update Project***\nEnter Index:\"");
        @NotNull final Integer index = TerminalUtil.nextNumber() - 1;
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByIndex(session, index);
        System.out.println("Enter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Project Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().updateProjectByIndex(session, index, name, description);
    }

    @Override
    @NotNull
    public String name() {
        return "project-update-by-index";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
