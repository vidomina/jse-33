package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.exception.entity.ProjectNotFoundException;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Optional;

public class ProjectRemoveByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Remove project by name.";
    }

    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("***Remove Project***\nEnter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @Nullable final Project project = endpointLocator.getProjectEndpoint().removeProjectByName(session, name);
        Optional.ofNullable(project).orElseThrow(ProjectNotFoundException::new);

    }

    @Override
    @NotNull
    public String name() {
        return "project-remove-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
