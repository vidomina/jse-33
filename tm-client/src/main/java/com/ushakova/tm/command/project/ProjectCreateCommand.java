package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCreateCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Create new project.";
    }

    public void execute() {
        final @NotNull Session session = endpointLocator.getSession();
        System.out.println("***Project Create***");
        System.out.println("Enter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("Enter Description:");
        @NotNull final String description = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().createProject(session, name, description);
    }

    @Override
    @NotNull
    public String name() {
        return "project-create";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
