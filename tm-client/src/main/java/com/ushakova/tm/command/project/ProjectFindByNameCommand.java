package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Project;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectFindByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Show project by name";
    }

    public void execute() {
        @Nullable final Session session = endpointLocator.getSession();
        System.out.println("***Show Project***\nEnter Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        @NotNull final Project project = endpointLocator.getProjectEndpoint().findProjectByName(session, name);
        showProjectInfo(project);
    }

    @Override
    @NotNull
    public String name() {
        return "project-view-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
