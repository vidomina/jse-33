package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectClearCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Clear all projects.";
    }

    public void execute() {
        final @NotNull Session session = endpointLocator.getSession();
        System.out.println("*Project Clear*");
        endpointLocator.getProjectEndpoint().clearProject(session);
    }

    @Override
    @NotNull
    public String name() {
        return "project-clear";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
