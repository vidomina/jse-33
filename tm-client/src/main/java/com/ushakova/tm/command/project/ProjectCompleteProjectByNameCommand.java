package com.ushakova.tm.command.project;

import com.ushakova.tm.command.AbstractProjectCommand;
import com.ushakova.tm.endpoint.Role;
import com.ushakova.tm.endpoint.Session;
import com.ushakova.tm.util.TerminalUtil;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

public class ProjectCompleteProjectByNameCommand extends AbstractProjectCommand {

    @Override
    @Nullable
    public String arg() {
        return null;
    }

    @Override
    @Nullable
    public String description() {
        return "Set \"Complete\" status to project by name.";
    }

    public void execute() {
        final @NotNull Session session = endpointLocator.getSession();
        System.out.println("***Set Status \"Complete\" to Project***\nEnter Project Name:");
        @NotNull final String name = TerminalUtil.nextLine();
        endpointLocator.getProjectEndpoint().completeProjectByName(session, name);
    }

    @Override
    @NotNull
    public String name() {
        return "complete-project-by-name";
    }

    @Override
    @Nullable
    public Role[] roles() {
        return Role.values();
    }

}
