package com.ushakova.tm.util;

import com.ushakova.tm.exception.system.IncorrectIndexException;
import org.jetbrains.annotations.NotNull;

import java.util.Scanner;

public interface TerminalUtil {

    @NotNull
    Scanner SCANNER = new Scanner(System.in);

    @NotNull
    static String nextLine() {
        return SCANNER.nextLine();
    }

    @NotNull
    static Integer nextNumber() {
        @NotNull final String line = nextLine();
        try {
            return Integer.parseInt(line);
        } catch (Exception e) {
            throw new IncorrectIndexException(line);
        }
    }

}
