package com.ushakova.tm.repository;

import com.ushakova.tm.api.repository.ICommandRepository;
import com.ushakova.tm.command.AbstractCommand;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

public class CommandRepository implements ICommandRepository {

    @NotNull
    private final List<AbstractCommand> list = new ArrayList<>();

    @Override
    public void add(@NotNull final AbstractCommand command) {
        list.add(command);
    }

    @Override
    @NotNull
    public Collection<String> getArguments() {
        @NotNull final List<String> args = new ArrayList<>();
        for (@NotNull final AbstractCommand command : list) {
            if (command.arg() != null)
                args.add(command.arg());
        }
        return args;
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByArg(@NotNull final String arg) {
        return list.stream()
                .filter(c -> arg.equals(c.arg()))
                .findFirst().orElse(null);
    }

    @Override
    @Nullable
    public AbstractCommand getCommandByName(@NotNull final String name) {
        return list.stream()
                .filter(c -> c.name().equals(name))
                .findFirst().orElse(null);
    }

    @Override
    public Collection<AbstractCommand> getCommandHasArgs() {
        return list.stream().filter(c -> c.arg() != null).collect(Collectors.toList());
    }

    @Override
    @NotNull
    public Collection<String> getCommandNames() {
        @Nullable final List<String> names = new ArrayList<>();
        for (@NotNull final AbstractCommand command : list) {
            names.add(command.name());
        }
        return names;
    }

    @Override
    @NotNull
    public Collection<AbstractCommand> getCommands() {
        return list;
    }

}