package com.ushakova.tm.bootstrap;

import com.ushakova.tm.api.repository.IProjectRepository;
import com.ushakova.tm.api.repository.ISessionRepository;
import com.ushakova.tm.api.repository.ITaskRepository;
import com.ushakova.tm.api.repository.IUserRepository;
import com.ushakova.tm.api.service.*;
import com.ushakova.tm.component.Backup;
import com.ushakova.tm.endpoint.*;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.repository.ProjectRepository;
import com.ushakova.tm.repository.SessionRepository;
import com.ushakova.tm.repository.TaskRepository;
import com.ushakova.tm.repository.UserRepository;
import com.ushakova.tm.service.*;
import com.ushakova.tm.util.SystemUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.ws.Endpoint;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Bootstrap implements IServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository();

    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @NotNull
    private final IProjectTaskService projectTaskService = new ProjectTaskService(taskRepository, projectRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final IPropertyService propertyService = new PropertyService();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @NotNull
    private final IUserService userService = new UserService(userRepository, propertyService);

    @NotNull
    private final IAuthService authService = new AuthService(userService, propertyService);

    @NotNull
    private final ISessionService sessionService = new SessionService(userService, sessionRepository, propertyService);

    @NotNull
    private final ProjectEndpoint projectEndpoint = new ProjectEndpoint(projectService, sessionService);

    @NotNull
    private final TaskEndpoint taskEndpoint = new TaskEndpoint(taskService, sessionService);

    @NotNull
    private final UserEndpoint userEndpoint = new UserEndpoint(sessionService, userService);

    @NotNull
    private final IDataService dataService = new DataService(projectService, taskService, userService);
    @NotNull
    private final AdminUserEndpoint adminUserEndpoint = new AdminUserEndpoint();
    @NotNull
    private final SessionEndpoint sessionEndpoint = new SessionEndpoint(sessionService);
    @NotNull
    public Backup backup = new Backup(this, propertyService, dataService);
    @NotNull
    private final AdminDataEndpoint adminDataEndpoint = new AdminDataEndpoint(sessionService, backup, dataService);

    @Override
    @NotNull
    public IAuthService getAuthService() {
        return authService;
    }

    @Override
    @NotNull
    public IProjectService getProjectService() {
        return projectService;
    }

    @Override
    public @NotNull
    IProjectTaskService getProjectTaskService() {
        return projectTaskService;
    }

    @NotNull
    public IPropertyService getPropertyService() {
        return propertyService;
    }

    @Override
    @NotNull
    public ITaskService getTaskService() {
        return taskService;
    }

    @Override
    @NotNull
    public IUserService getUserService() {
        return userService;
    }

    private void initData() {
        @NotNull final String guestId = getUserService().add("Guest", "Guest", "wholah0@baidu.com").getId();
        @NotNull final String adminId = getUserService().add("Admin", "Admin", Role.ADMIN).getId();
        projectService.add("Commodo At Libero Associates", "Vel facilisis volutpat est velit egestas dui.", adminId).setStatus(Status.COMPLETE);
        projectService.add("Feugiat LLC", "Tellus in metus vulputate eu scelerisque.", adminId).setStatus(Status.IN_PROGRESS);
        projectService.add("Aenean LLP", "Fermentum leo vel orci porta non pulvinar.", guestId).setStatus(Status.IN_PROGRESS);
        projectService.add("Non Leo Incorporated", "Sit amet justo donec enim diam vulputate ut.", guestId).setStatus(Status.NOT_STARTED);
        projectService.add("Dignissim Tempor Arcu Limited", "Et leo duis ut diam.", guestId).setStatus(Status.COMPLETE);
        projectService.add("Nec Tempus Corp", "Vitae semper quis lectus nulla at.", guestId).setStatus(Status.NOT_STARTED);

        taskService.add("Integer Company", "Enim lobortis scelerisque fermentum dui faucibus.", adminId).setStatus(Status.COMPLETE);
        taskService.add("Penatibus Inc", "Sed adipiscing diam donec adipiscing tristique.", adminId).setStatus(Status.NOT_STARTED);
        taskService.add("At Pretium LLC", "Erat imperdiet sed euismod nisi porta lorem mollis.", adminId).setStatus(Status.IN_PROGRESS);
        taskService.add("Risus Donec Inc", "Pretium fusce id velit ut tortor pretium viverra.", guestId).setStatus(Status.NOT_STARTED);
        taskService.add("Eu Dolor Corporation", "Lacinia quis vel eros donec ac odio tempor.", guestId).setStatus(Status.IN_PROGRESS);
        taskService.add("Phasellus Libero Institut", "Mauris sit amet massa vitae tortor.", guestId).setStatus(Status.NOT_STARTED);
    }

    private void initEndpoint() {
        registry(adminUserEndpoint);
        registry(projectEndpoint);
        registry(taskEndpoint);
        registry(userEndpoint);
        registry(adminDataEndpoint);
        registry(sessionEndpoint);
    }

    @SneakyThrows
    public void initPID() {
        try {
            @NotNull final String fileName = "task-manager.pid";
            @NotNull final String pid = Long.toString(SystemUtil.getPID());
            Files.write(Paths.get(fileName), pid.getBytes());
            @NotNull final File file = new File(fileName);
            file.deleteOnExit();
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }

    private void registry(final @NotNull Object endpoint) {
        final String host = propertyService.getServerHost();
        final String port = propertyService.getServerPort();
        final String name = endpoint.getClass().getSimpleName();
        final String wsdl = "http://" + host + ":" + port + "/" + name + "?wsdl";
        System.out.println(wsdl);
        Endpoint.publish(wsdl, endpoint);
    }

    public void start(@Nullable final String... args) {
        initData();
        initEndpoint();
        backup.init();
    }


}
