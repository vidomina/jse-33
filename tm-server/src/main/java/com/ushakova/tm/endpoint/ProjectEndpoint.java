package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.IProjectEndpoint;
import com.ushakova.tm.api.service.IProjectService;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Project;
import com.ushakova.tm.model.Session;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService
public class ProjectEndpoint extends AbstractEndpoint implements IProjectEndpoint {

    private IProjectService projectService;

    private ISessionService sessionService;

    public ProjectEndpoint() {

    }

    public ProjectEndpoint(final IProjectService projectService, ISessionService sessionService) {
        this.projectService = projectService;
        this.sessionService = sessionService;
    }

    @Override
    @WebMethod
    public void addAllProjects(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "entities") final List<Project> entities
    ) {
        sessionService.validate(session);
        projectService.addAll(session.getUserId(), entities);
    }

    @Override
    @WebMethod
    public void addProject(@Nullable @WebParam(name = "session") final Session session,
                           @Nullable @WebParam(name = "entity") final Project entity
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusById(id, status, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByIndex(index, status, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project changeProjectStatusByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return projectService.changeStatusByName(name, status, session.getUserId());
    }

    @Override
    @WebMethod
    public void clearProject(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        projectService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.completeById(id, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.completeByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project completeProjectByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.completeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void createProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull List<Project> findAllProjects(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        return Objects.requireNonNull(projectService.findAll(session.getUserId()));
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.findByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project findProjectByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.findByName(name, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project removeProjectById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.removeById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project removeProjectByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return Objects.requireNonNull(projectService.removeByIndex(index, session.getUserId()));
    }

    @Override
    @WebMethod
    public @NotNull Project removeProjectByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return Objects.requireNonNull(projectService.removeByName(name, session.getUserId()));
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return projectService.startById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return projectService.startByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Project startProjectByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return projectService.startByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void updateProjectById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.updateById(session.getUserId(), id, name, description);
    }

    @Override
    @WebMethod
    public void updateProjectByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @NotNull @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        projectService.updateByIndex(index, name, description, session.getUserId());
    }

}