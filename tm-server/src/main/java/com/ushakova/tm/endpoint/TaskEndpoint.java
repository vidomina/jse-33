package com.ushakova.tm.endpoint;

import com.ushakova.tm.api.endpoint.ITaskEndpoint;
import com.ushakova.tm.api.service.IProjectTaskService;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.api.service.ITaskService;
import com.ushakova.tm.enumerated.Status;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.Task;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;
import java.util.Objects;

@WebService
public class TaskEndpoint extends AbstractEndpoint implements ITaskEndpoint {

    private ITaskService taskService;

    private IProjectTaskService projectTaskService;

    private ISessionService sessionService;

    public TaskEndpoint() {
    }

    public TaskEndpoint(final ITaskService taskService, ISessionService sessionService, IProjectTaskService projectTaskService) {
        this.taskService = taskService;
        this.sessionService = sessionService;
        this.projectTaskService = projectTaskService;

    }

    public TaskEndpoint(ITaskService taskService, ISessionService sessionService) {

    }

    @Override
    @WebMethod
    public void add(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "entity") final Task entity
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), entity);
    }

    @Override
    @WebMethod
    public void addAll(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "entities") final List<Task> entities
    ) {
        sessionService.validate(session);
        taskService.addAll(session.getUserId(), entities);
    }

    @WebMethod
    public @Nullable Task bindTaskToProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId,
            @Nullable @WebParam(name = "projectId") final String projectId
    ) {
        sessionService.validate(session);
        return projectTaskService.bindTaskByProject(session.getUserId(), taskId, projectId);
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusById(id, status, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByIndex(index, status, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task changeStatusByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "status") final Status status
    ) {
        sessionService.validate(session);
        return taskService.changeStatusByName(name, status, session.getUserId());
    }

    @Override
    @WebMethod
    public void clear(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        taskService.clear(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task completeById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.completeById(id, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task completeByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.completeByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task completeByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.completeByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public void create(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.add(session.getUserId(), name, description);
    }

    @Override
    @WebMethod
    public @NotNull List<Task> findAll(@Nullable @WebParam(name = "session") final Session session) {
        sessionService.validate(session);
        return taskService.findAll(session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.findById(session.getUserId(), id);
    }

    @Override
    @WebMethod
    public @Nullable Task findByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.findByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task findByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.findByName(session.getUserId(), name);
    }

    @Override
    @WebMethod
    public @Nullable Task removeById(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.removeById(id, session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Task removeByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @NotNull @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.removeByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task removeByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return Objects.requireNonNull(taskService.removeByName(name, session.getUserId()));
    }

    @Override
    @WebMethod
    public @NotNull Task startById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id
    ) {
        sessionService.validate(session);
        return taskService.startById(id, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task startByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index
    ) {
        sessionService.validate(session);
        return taskService.startByIndex(index, session.getUserId());
    }

    @Override
    @WebMethod
    public @NotNull Task startByName(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "name") final String name
    ) {
        sessionService.validate(session);
        return taskService.startByName(name, session.getUserId());
    }

    @Override
    @WebMethod
    public @Nullable Task unbindTaskFromProject(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "taskId") final String taskId
    ) {
        sessionService.validate(session);
        return projectTaskService.unbindTaskFromProject(session.getUserId(), taskId);
    }

    @Override
    @WebMethod
    public void updateById(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "id") final String id,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.updateById(id, name, description, session.getUserId());
    }

    @Override
    @WebMethod
    public void updateByIndex(
            @Nullable @WebParam(name = "session") final Session session,
            @Nullable @WebParam(name = "index") final Integer index,
            @Nullable @WebParam(name = "name") final String name,
            @Nullable @WebParam(name = "description") final String description
    ) {
        sessionService.validate(session);
        taskService.updateByIndex(index, name, description, session.getUserId());
    }

}