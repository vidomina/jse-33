package com.ushakova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ushakova.tm.api.repository.ISessionRepository;
import com.ushakova.tm.api.service.IPropertyService;
import com.ushakova.tm.api.service.ISessionService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.enumerated.Role;
import com.ushakova.tm.exception.auth.AccessDeniedException;
import com.ushakova.tm.model.Session;
import com.ushakova.tm.model.User;
import com.ushakova.tm.util.HashUtil;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import static org.reflections.util.Utils.isEmpty;

public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final IUserService userService;

    @NotNull
    private final ISessionRepository sessionRepository;

    @NotNull
    private final IPropertyService propertyService;

    public SessionService(@NotNull final IUserService userService,
                          @NotNull final ISessionRepository sessionRepository, @NotNull IPropertyService propertyService) {
        super(sessionRepository);
        this.userService = userService;
        this.sessionRepository = sessionRepository;
        this.propertyService = propertyService;
    }

    @Override
    public boolean checkDataAccess(@NotNull String login, @NotNull String password) {
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final String passwordHash = HashUtil.salt(propertyService, password);
        return passwordHash.equals(user.getPasswordHash());
    }

    @Override
    public boolean close(@NotNull Session session) {
        validate(session);
        remove(session);
        return false;
    }

    @Override
    @NotNull
    public Session open(@NotNull final String login, @NotNull final String password) {
        final boolean check = checkDataAccess(login, password);
        @NotNull final User user = userService.findByLogin(login);
        @NotNull final Session session = new Session();
        session.setUserId(user.getId());
        session.setTimestamp(System.currentTimeMillis());
        sessionRepository.add(session);
        return sign(session);
    }

    @SneakyThrows
    @Override
    @NotNull
    public Session sign(@NotNull final Session session) {
        session.setSignature(null);
        @NotNull final String salt = propertyService.getSessionSecret();
        final int cycle = propertyService.getSessionIteration();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writeValueAsString(session);
        @NotNull final String signature = HashUtil.salt(salt, cycle, json);
        session.setSignature(signature);
        return session;
    }

    @Override
    public void validate(@NotNull final Session session) {
        if (isEmpty(session.getSignature())) throw new AccessDeniedException();
        if (isEmpty(session.getUserId())) throw new AccessDeniedException();
        if (session.getTimestamp() == null) throw new AccessDeniedException();
        @NotNull final Session temp = session.clone();
        if (temp == null) throw new AccessDeniedException();
        @NotNull final String signatureSource = session.getSignature();
        @NotNull final Session sessionSign = sign(temp);
        @Nullable final String signatureTarget = sessionSign.getSignature();
        final boolean check = signatureSource.equals(signatureTarget);
        if (!check) throw new AccessDeniedException();
        if (!sessionRepository.contains(session.getId())) throw new AccessDeniedException();
    }

    @Override
    public void validate(@NotNull Session session, @NotNull Role role) {
        validate(session);
        @NotNull final String userId = session.getUserId();
        @NotNull final User user = userService.findById(userId);
        if (!role.equals(user.getRole())) throw new AccessDeniedException();
    }

}