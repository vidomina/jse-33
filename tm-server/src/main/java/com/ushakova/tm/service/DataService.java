package com.ushakova.tm.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.fasterxml.jackson.dataformat.yaml.YAMLFactory;
import com.ushakova.tm.api.service.IDataService;
import com.ushakova.tm.api.service.IProjectService;
import com.ushakova.tm.api.service.ITaskService;
import com.ushakova.tm.api.service.IUserService;
import com.ushakova.tm.component.Backup;
import com.ushakova.tm.dto.Domain;
import com.ushakova.tm.exception.empty.EmptyDomainException;
import lombok.SneakyThrows;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Base64;

public class DataService implements IDataService {

    @NotNull
    private final IProjectService projectService;

    @NotNull
    private final ITaskService taskService;

    @NotNull
    private final IUserService userService;

    public DataService(@NotNull IProjectService projectService, @NotNull ITaskService taskService, @NotNull IUserService userService) {
        this.projectService = projectService;
        this.taskService = taskService;
        this.userService = userService;
    }

    @NotNull
    public Domain getDomain() {
        @NotNull final Domain domain = new Domain();
        domain.setProjects(projectService.findAll());
        domain.setTasks(taskService.findAll());
        domain.setUsers(userService.findAll());
        return domain;
    }

    public void setDomain(@Nullable final Domain domain) {
        if (domain == null) throw new EmptyDomainException();
        projectService.clear();
        projectService.addAll(domain.getProjects());
        taskService.clear();
        taskService.addAll(domain.getTasks());
        userService.clear();
        userService.addAll(domain.getUsers());
    }

    @Override
    @SneakyThrows
    public void loadBackup() {
        @NotNull final File file = new File(Backup.BACKUP_XML);
        if (!file.exists()) return;
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(Backup.BACKUP_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataBase64() {
        @NotNull final String base64Data = new String(Files.readAllBytes(Paths.get(Backup.FILE_BASE64)));
        final byte[] decodedData = Base64.getDecoder().decode(base64Data);
        @NotNull final ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(decodedData);
        @NotNull final ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
        @NotNull final Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        byteArrayInputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataBin() {
        @NotNull FileInputStream fileInputStream = new FileInputStream(Backup.FILE_BINARY);
        @NotNull ObjectInputStream objectInputStream = new ObjectInputStream(fileInputStream);
        @NotNull Domain domain = (Domain) objectInputStream.readObject();
        setDomain(domain);
        objectInputStream.close();
        fileInputStream.close();
    }

    @Override
    @SneakyThrows
    public void loadDataJson() {
        @NotNull final String json = new String(Files.readAllBytes(Paths.get(Backup.FILE_FASTERXML_JSON)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final Domain domain = objectMapper.readValue(json, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataJsonJaxB() {
        System.setProperty(Backup.SYSTEM_JSON_PROPERTY_NAME, Backup.SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final File file = new File(Backup.FILE_JAXB_JSON);
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(Backup.JAXB_JSON_PROPERTY_NAME, Backup.JAXB_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataXml() {
        @NotNull final String xml = new String(Files.readAllBytes(Paths.get(Backup.FILE_FASTERXML_XML)));
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final Domain domain = objectMapper.readValue(xml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void loadDataXmlJaxB() {
        @NotNull final File file = new File(Backup.FILE_JAXB_XML);
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        @NotNull final Domain domain = (Domain) unmarshaller.unmarshal(file);
        setDomain(domain);

    }

    @Override
    @SneakyThrows
    public void loadDataYaml() {
        @NotNull final String yaml = new String(Files.readAllBytes(Paths.get(Backup.FILE_FASTERXML_YAML)));
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final Domain domain = objectMapper.readValue(yaml, Domain.class);
        setDomain(domain);
    }

    @Override
    @SneakyThrows
    public void saveBackup() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.BACKUP_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBase64() {
        @NotNull final Domain domain = getDomain();

        @NotNull final File file = new File(Backup.FILE_BASE64);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        byteArrayOutputStream.close();
        final byte[] bytes = byteArrayOutputStream.toByteArray();
        @NotNull final String base64 = Base64.getEncoder().encodeToString(bytes);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_BASE64);
        fileOutputStream.write(base64.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataBin() {
        @NotNull final Domain domain = getDomain();
        @NotNull final File file = new File(Backup.FILE_BINARY);
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(file);
        @NotNull final ObjectOutputStream objectOutputStream = new ObjectOutputStream(fileOutputStream);
        objectOutputStream.writeObject(domain);
        objectOutputStream.close();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJson() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        @NotNull final String json = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_FASTERXML_JSON);
        fileOutputStream.write(json.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataJsonJaxB() {
        System.setProperty(Backup.SYSTEM_JSON_PROPERTY_NAME, Backup.SYSTEM_JSON_PROPERTY_VALUE);
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Backup.JAXB_JSON_PROPERTY_NAME, Backup.JAXB_JSON_PROPERTY_VALUE);
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_JAXB_JSON);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataXml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new XmlMapper();
        @NotNull final String xml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_FASTERXML_XML);
        fileOutputStream.write(xml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataXmlJaxB() {
        @NotNull final Domain domain = getDomain();
        @NotNull final JAXBContext jaxbContext = JAXBContextFactory
                .createContext(new Class[]{Domain.class}, null);
        @NotNull final Marshaller jaxbMarshaller = jaxbContext.createMarshaller();
        jaxbMarshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_JAXB_XML);
        jaxbMarshaller.marshal(domain, fileOutputStream);
        fileOutputStream.flush();
        fileOutputStream.close();
    }

    @Override
    @SneakyThrows
    public void saveDataYaml() {
        @NotNull final Domain domain = getDomain();
        @NotNull final ObjectMapper objectMapper = new ObjectMapper(new YAMLFactory());
        @NotNull final String yaml = objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(domain);
        @NotNull final FileOutputStream fileOutputStream = new FileOutputStream(Backup.FILE_FASTERXML_YAML);
        fileOutputStream.write(yaml.getBytes());
        fileOutputStream.flush();
        fileOutputStream.close();
    }

}
