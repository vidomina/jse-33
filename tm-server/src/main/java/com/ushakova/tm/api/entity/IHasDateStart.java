package com.ushakova.tm.api.entity;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.util.Date;

public interface IHasDateStart {

    @Nullable
    Date getDateStart();

    void setDateStart(@NotNull Date dateStart);

}
